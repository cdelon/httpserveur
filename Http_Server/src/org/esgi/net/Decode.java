package org.esgi.net;

import java.io.BufferedReader;
import java.io.IOException;

import org.esgi.net.interfaces.IRequest;


public class Decode {

	
	

	
	public IRequest parseRequest(BufferedReader reader) throws NumberFormatException, IOException
	{
		Request request = new Request();
		String line;
		String str[];
		
		while(!(line = reader.readLine()).isEmpty()) {
			str=line.split(" ");
			
			if(str[0].contains("GET")){
				request.setRequestURL(str[1]);
				request.setProtocol(str[2].substring(0, str[2].indexOf('/')));
				request.setHttpVersion(str[2].substring(str[2].indexOf('/')+1));
			}
			
			if(str[0].contains("Host"))
			{
				request.setHost(str[1].substring(0,str[1].indexOf(':')));
				request.setPort(Integer.parseInt(str[1].substring(str[1].indexOf(':')+1)));
			}
			
			str=null;
   			//System.out.println(line);
		}
		
		/*
		System.out.println("requestUrl:"+request.getRequestURL());
		System.out.println("host:"+request.getHost());
		System.out.println("httpversion:"+request.getHttpVersion());
		System.out.println("port:"+request.getPort());
		System.out.println("protocol:"+request.getProtocol());
		*/
		
		return request;
	}

}
