package org.esgi.net;

import java.io.File;

public class Explorer {
	
	private String root = "./www";
	//private String pathTest = "";
	File f;
	
	public Explorer(String path)
	{
		f = new File(root+path);
	}
	
	public String testCase()
	{
		if(f.exists())
		{
			if(f.isDirectory())
			{
				return "directory";
			}
			else
			{
				return "file";
			}
		}
		else
		{
			return "404";
		}
	}
	
	public String getStatus()
	{
		if(f.exists())
		{
			return "200";
		}
		else
		{
			return "404";
		}
	}
	
	public String getContentOfDirectory()
	{
		if(f.exists())
		{
			if(f.isDirectory())
			{
				File[] content = f.listFiles();
				StringBuilder sb = new StringBuilder();
				sb.append("<ul>");
				for(int cpt=0;cpt<content.length;cpt++)
				{
					if(content[cpt].isDirectory())
						sb.append("<li>");
						sb.append("<a href=\"/"+content[cpt].getName()+"\">"+content[cpt].getName()+"</a>");
						sb.append("</li>");
				}
				sb.append("</ul>");
				return sb.toString();
			}
			else
			{
				return null;
			}
		}
		else
		{
			return null;
		}
	}
}
