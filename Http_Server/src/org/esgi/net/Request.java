package org.esgi.net;

import java.util.HashMap;
import java.util.Map;

import org.esgi.net.interfaces.IRequest;


public class Request implements IRequest {

	Map<String, Object> parameters = new HashMap<String, Object>();
	
	private String realPath;
	private String parameter;
	private String header;
	private String requestURL;
	private String httpVersion;
	private String protocol;
	private String host;
	private int port;
	private String[] parameterskeys;
	private String method; 
	private String[] headerskeys;
	
	/*
	Exemple:
	GET http://www.eric-pidoux.com HTTP/1.0
	Accept : text/html
	If-Modified-Since : Saturday, 26-March-2010 08:17:01 GMT
	User-Agent : Mozilla/5.0
	[ CORPS DE LA REQUETE ]
		 */
	
	public void setMethod(String method) {
		this.method = method;
	}

	public void setParameterskeys(String[] parameterskeys) {
		this.parameterskeys = parameterskeys;
	}

	@Override
	public String getRealPath() {
		// TODO Auto-generated method stub
		return realPath;
	}

	@Override
	public String getParameter(String key) {
		// TODO Auto-generated method stub
		return parameter;
	}

	@Override
	public String getHeader(String key) {
		// TODO Auto-generated method stub
		return header;
	}

	@Override
	public String getRequestURL() {
		// TODO Auto-generated method stub
		return requestURL;
	}

	@Override
	public String getHttpVersion() {
		// TODO Auto-generated method stub
		return httpVersion;
	}

	@Override
	public String getProtocol() {
		// TODO Auto-generated method stub
		return protocol;
	}

	@Override
	public String getHost() {
		// TODO Auto-generated method stub
		return host;
	}

	@Override
	public int getPort() {
		// TODO Auto-generated method stub
		return port;
	}
	

	public void setRealPath(String str){
		realPath=str;
	}
	
	public void setParameter(String str){
		parameter=str;
	}
	
	public void setHeader(String str){
		header=str;
	}
	
	public void setRequestURL(String str){
		requestURL=str;
	}	
	
	public void setHttpVersion(String str){
		httpVersion=str;
	}
	
	public void setProtocol(String str){
		protocol=str;
	}
	
	public void setHost(String str){
		host=str;
	}
	
	public void setPort(int str){
		port=str;
	}

	@Override
	public String getMethod() {
		// TODO Stub de la méthode généré automatiquement
		return method;
	}

	@Override
	public String[] getParametersKeys() {
		// TODO Stub de la méthode généré automatiquement
		return parameterskeys;
	}

	@Override
	public String[] getHeadersKeys() {
		// TODO Stub de la méthode généré automatiquement
		return headerskeys;
	}

	public void setHeaderskeys(String[] headerskeys) {
		this.headerskeys = headerskeys;
	}
}
