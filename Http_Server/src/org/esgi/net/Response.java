package org.esgi.net;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.esgi.net.interfaces.IResponse;


public class Response implements IResponse {

	
	/*
	 * (non-Javadoc)
	 * @see org.essilab.net.IRequest#getRealPath()
	 */
	
	
	Map<String,String> headers;
	String body;

	/*
	Exemple:
	HTTP/1.0 200 OK
	Date : Saturday, 26-March-2010 08:17:01 GMT
	Server : Apache
	Content-Type : text/HTML
	Content-Length : 105
	Last-Modified : Fri, 14 Jan 2000 08:25:13 GMT
	[ CORPS DE LA REPONSE ]
		 */
	
	public String getBody() {
		return body;
	}


	public void setBody(String body) {
		this.body = body;
	}


	public Response()
	{
		headers = new HashMap<String,String>();
	}
	

	public String getHeader(String key) {
		return headers.get(key);
	}
	
	
	@Override
	public void setHeader(String key, String value) {
		headers.put(key, value);
	}
	
	@Override
	public OutputStream getOutputStream() throws IOException {
		
		OutputStream os = new ByteArrayOutputStream();
		PrintWriter pw = new PrintWriter(os);
		String CRLF = "\r\n";
		
		//Status-Line
		pw.write(headers.get("httpVersion"));
		pw.write(" ");
		pw.write(headers.get("status"));
		pw.write(" ");
		if ("200" == headers.get("status"))
			pw.write("OK");
		else
			pw.write("NOT FOUND");
		pw.write(CRLF);
		
		//General-Header
		pw.write("Date : ");
		pw.write(new Date(System.currentTimeMillis()).toString());
		pw.write(CRLF);
		
		//Response-Header
		pw.write("Server : Apache");
		pw.write(CRLF);
		
		pw.write("Content-Type : ");
		pw.write(headers.get("content-type"));
		pw.write(CRLF);
		
		pw.write("Content-Length : ");
		pw.write(headers.get("content-length"));
		pw.write(CRLF);
		
		pw.write(body);
		pw.write(CRLF);
		
		
		// TODO Auto-generated method stub
		return os;
	}

	@Override
	public void flush() {
		// TODO Auto-generated method stub
	}	
}