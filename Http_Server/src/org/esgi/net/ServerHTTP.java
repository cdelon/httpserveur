package org.esgi.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import org.esgi.net.interfaces.IHttpHandler;
import org.esgi.net.interfaces.IResponse;


public class ServerHTTP {

	/**
	 * @param args
	 * @throws IOException 
	 */
	private static final int PORT = 8000;
    private static final String HOST = "127.0.0.1";
   
    ServerSocket serverSocket;
    
    
    public ServerHTTP() throws IOException {
		serverSocket = new ServerSocket(PORT);
	}
    
    public void start() throws IOException{
    	
    	Socket clientSocket;
    	
    	while (true) {
    		clientSocket = serverSocket.accept();
    		BufferedReader reader = new BufferedReader(new InputStreamReader( clientSocket.getInputStream()));
    		String line;
    		String str[];
			// Cr�er l'object request et d�coder la requete (dans socket.getInputStream)*
			//IRequest request = null; // A toi jouer...
			
			Request request = new Request();
			
			request.setPort(PORT);
			request.setHost(HOST);
			
			while(!(line = reader.readLine()).isEmpty()) {
    			System.out.println(line);
				
				str=line.split(" ");
				
    			if(str[0].contains("GET")){
    				request.setRequestURL(str[1]);
    				request.setProtocol(str[2]);
    			}
    			if(str[0]=="Host:"){
    				request.setHost(str[1]);
    			}
    			
    			str=null;
    		}
    		// Cr�er l'object response 
			//IResponse response = null; // A toi jouer...
			Response response = new Response();
			// invoquer la m�thode service du handler.
			//IHttpHandler handler = null; // A toi d jouer
			HttpHandler handler = new HttpHandler();
			handler.service(request, response);
			// Send response.
			Explorer explorer = new Explorer(request.getRequestURL());
			PrintWriter writer = new PrintWriter(response.getOutputStream());
			writer.print(request.getProtocol()+"/"+request.getHttpVersion()+" "+explorer.getStatus()+" OK\n");
			// Writes headers to pw.
			//writer.print();
			// Write content to pw.
			
			if (null!=handler)
				handler.service(request, response);
			
    	}
    }
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		// Cr�er serveur
		// Attendre requete client.
		
		ServerHTTP server=new ServerHTTP();
        server.start();		                                                             
		
	}

}
