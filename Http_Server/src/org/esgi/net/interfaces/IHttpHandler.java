package org.esgi.net.interfaces;

import java.io.IOException;

public interface IHttpHandler {

	public void service(IRequest request, IResponse response) throws IOException;
}
