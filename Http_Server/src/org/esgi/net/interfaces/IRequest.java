package org.esgi.net.interfaces;

public interface IRequest {
	public String getRealPath();
	public String getParameter(String key);
	public String getHost();
	public int getPort();
	public String getHeader(String key);
	public String getRequestURL();
	public String getHttpVersion();
	public String getProtocol();
	public String getMethod();
	public String[] getParametersKeys();
	public String[] getHeadersKeys();
}