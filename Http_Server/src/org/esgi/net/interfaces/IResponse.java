package org.esgi.net.interfaces;

import java.io.IOException;
import java.io.OutputStream;

public interface IResponse {

	public void setHeader(String key, String value);
	/**
	 * An header can't be sent after outputstream has been used.
	 * @throws IOException 
	 */
	public OutputStream getOutputStream() throws IOException;
	public void flush();
	
}
